var d = 100;
var c =1;
var al = 0;
var alamount = 1.3;

function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(255);

// to make the circle expand/shrink
  d += c;
  if (d > 300){
		c = -c;
  } else if (d < 80){
    c = -c;
  }

  //fade
  if (d<90) alamount=1;

  if (d>290) alamount=-1;
  al += alamount

// to make it slow down at max and min
  if (d < 81) {
    frameRate(10);
  } else if (d > 299) {

   frameRate(10);
  } else {
   frameRate(60);
  }


  //light blue circle
   fill(177, 240, 250);
  ellipse (200,200,300);

  //dark blue circle
  fill(109, 196, 209);
  noStroke();
  ellipse(200,200,d);

  // the text
   if (c == 1) {
     textSize(22);
     fill(109, 196, 209,al);
     textAlign(CENTER);
   text("Breathe in", 200,380);
  } else if (c == -1) {
    fill(109, 196, 209,al);
    text("Breathe out",200,380);

  }
}
