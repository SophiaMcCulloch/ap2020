![screenShot](Skærmbillede_2020-02-07_kl._12.48.53.png)


https://sophiamcculloch.gitlab.io/ap2020/Miniex1/

For my first miniex I have made an animation of a moon in it’s different stages continually travelling diagonally across the screen. The moon is surrounded by small flashing stars. 

I would describe my first independent coding experience as challenging, but at the same time a very rewarding process, as I felt it was a very satisfying feeling to come up with an idea for an image/animation and then see it materialise on the screen. I have mostly used TheCodingTrain’s video’s as reference for the code that I wrote, and also the p5.js reference page. 
I have always seen programming and coding as a very specialized field that was not for me, but I have learned now that coding can be for everyone, and that you don't have to be a software professional to create something out of code. 
 
My impression of learning to code is that it is in many ways very similar to learning any other language. You have to get the syntax right and learn which words and terms apply to different functions. 
 
I was quite surprised to learn from Wendy Chun's text that women actually were the first real programmers, as I have always had the impression that programming in many ways is a very male dominated field. 
I can definitely resonate with Chun's argument that programming can produce strong sensations of power and control, as to have the ability to create feels very empowering. 

