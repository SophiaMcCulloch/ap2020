//for the stars
var i = 0

//the 1. moon circle
var circl = {
  x: 30,
  y: -30
};

//the 1. shadow circle
var circl2 = {
  x: -45,
  y: -64
};

//the 2. moon circle
var circl3 = {
  x: -80,
  y: -185
};

//the 2. shadow circle
var circl4 = {
  x: -210,
  y: -244
};

//the 3. moon circle
var circl5 = {
  x: -190,
  y: -340
};

//the 3. shadow circle
var circl6 = {
  x: -355,
  y: -415
};



function setup() {
  createCanvas(400, 400);
}

function draw() {

  background(0);

  //stars
  i = i + 1

  // every 10th time, the condition is true
  if (i % 10 === 0) {
    // fill with 50
    fill(50);
  } else {
    // all the oteher times, fill with 255
    fill(255);
  }

  circle(80, 150, 5);
  circle(50, 240, 5);
  circle(100, 300, 5);
  circle(250, 200, 5);
  circle(300, 80, 5);
  circle(299, 393, 5);
  circle(15, 50, 5);
  circle(160, 370, 5);
  circle(100, 200, 5);
  circle(80, 370, 5);
  circle(205, 14, 5);
  circle(380, 40, 5);
  circle(358, 240, 5);
  circle(13, 360, 5);

  //I make the 1. moon circle move across the canvas diagonally
  fill(255, 255, 237);
  ellipse(circl.x, circl.y, 65, 65);

  circl.x = circl.x + 1;
  circl.y = circl.y + 1.4;

  //to start the 1. moon circle over
  if (circl.x > height) {
    circl.x = 30, circl.y = -30;
    circl2.x = -45, circl2.y = -64;
  }

  //I make the 2. moon circle move across the canvas diagonally
  fill(255, 255, 237);
  ellipse(circl3.x, circl3.y, 65, 65);

  circl3.x = circl3.x + 1;
  circl3.y = circl3.y + 1.4;

  //to start the 2. moon circle and shadow circle over
  if (circl3.x > height) {
    circl3.x = 30, circl3.y = -30;
    circl4.x = -45, circl4.y = -64;
  }

  //I make the 3. moon circle move across the canvas diagonally
  fill(255, 255, 237);
  ellipse(circl5.x, circl5.y, 65, 65);

  circl5.x = circl5.x + 1;
  circl5.y = circl5.y + 1.4;

  //to start the 3. moon circle over
  if (circl5.x > height) {
    circl5.x = 30, circl5.y = -30;
    circl6.x = -45, circl6.y = -64;
  }




  // I make the 1. shadow circle move across the screen diagonally at a slightly faster speed to create the eclipse effect
  fill(0);
  ellipse(circl2.x, circl2.y, 60, 60);

  circl2.x = circl2.x + 1.44;
  circl2.y = circl2.y + 1.6;


  // I make the 2. shadow circle move diagonally across the screen
  fill(0);
  ellipse(circl4.x, circl4.y, 60, 60);

  circl4.x = circl4.x + 1.44;
  circl4.y = circl4.y + 1.6;



  // I make the 3. shadow circle move diagonally across the screen
  fill(0);
  ellipse(circl6.x, circl6.y, 60, 60);

  circl6.x = circl6.x + 1.44;
  circl6.y = circl6.y + 1.6;


}
