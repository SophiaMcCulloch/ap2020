https://sophiamcculloch.gitlab.io/ap2020/4cardmonte/

"4-Card Monte" is an interactive project that finds its aesthetics from the Windows 98 desktop where three windows are open: a game, Instagram, and the Notepad. The participant is invited to play the game. The goal is to locate one of the two red cards out of four cards that face-down. If the participant locates a red card, he/she wins that round and moves on to the next. If not, he/she loses. The first screen in the game is a set of instructions for the participant to follow, where it is stated that the chance to win a round is 50/50. But then, something (or someone) takes over the control of the desktop, as posts, followers, and followings start to disappear from the Instagram window, the cursor moves without interaction from the participant, and messages appear in the Notepad as if something (or someone) is writing.

On the surface it appears to be a simple card game, however it is quickly revealed that something darker is at play and that the card game is ultimately rigged.
This project is a comment on the control that technology has on us and our society. The power relations that are obfuscated in the incorporeal, virtual world become apparent and tangible, and thus the project invites the participant to feel what usually is actively concealed.


![screenShot](gruppe.jpg)