let button;
let ctracker;
let myFont;
let trash;
let computer;
let docs;
let docs1;
let web;
let start;
let game;
let game1;
let pointer;
let note;
let note1;
let notepad;
let gamew;
let game2;
let insta;
let redcard;
let blackcard;
let cardback;
let cardback1;
let insta1;
let input;

let cursorx;
let cursory;
let cursorstartx = 400;
let cursorstarty = 280;
let xspeed = 2;
let yspeed = 3;

let posts;
let followers;
let following;
let postsdown = -1;
let followersdown = -1;
let followingdown = -1;
let test = 0;

let wel = 0;
let bbb = 0;

function preload() {
  //font from https://yadi.sk/d/vRJRvCnjU-KKpA
  myFont = loadFont('assets/tekst.ttf');
  trash = loadImage('assets/trash.png');
  computer = loadImage('assets/computer.png');
  docs = loadImage('assets/docs.png');
  docs1 = loadImage('assets/docs.png');
  web = loadImage('assets/web.png');
  start = loadImage('assets/start1.png');
  pointer = loadImage('assets/pointer.png');
  game = loadImage('assets/game.png');
  game1 = loadImage('assets/game.png');
  note = loadImage('assets/note.png');
  note1 = loadImage('assets/note.png');
  notepad = loadImage('assets/notepad.png');
  gamew = loadImage('assets/notepad.png');
  game2 = loadImage('assets/game.png');
  insta = loadImage('assets/instaa.png');
  redcard = loadImage('assets/queenheart.jpg');
  blackcard = loadImage('assets/queenspar.png');
  cardback = loadImage('assets/cardback.jpg');
  cardback1 = loadImage('assets/cardback.jpg');
  insta1 = loadImage ('assets/insta1.png');
}


function setup() {
  createCanvas(windowWidth,windowHeight);
  noCursor();
  frameRate(20);

  input = createInput();
  input.position(windowWidth/2 + 77,windowHeight/2+10);

  button = createButton("Login");
  button.position(windowWidth/2 + 130,windowHeight/2+45);
  button.mousePressed(changeName);
}

function changeName(){

  test = 1;

  wel = 9;
}

function draw() {
  background(0, 128, 128);

  //Notepad
  notepad.resize(0,350);
  image(notepad,400,500);
  textFont(myFont);
  fill(0);
  textSize(9);


  //the game window
  gamew.resize(0,390);
  image(gamew,140,60);

  noStroke();
  fill(0,120,0);
  rect(144,100,634,345);

  fill(0,0,126);
  rect(145,65,150,14);

  fill(255);
  textSize(8);
  text('4-card monte',163,77);
  text('4-card monte',162,77);

  game2.resize(18,0);
  image(game2,145,63);

  //Instagram window created using PhotoShop
  //profile pic from https://www.pngitem.com/middle/ixwhRbb_bulbasaur-pixel-art-hd-png-download/
  insta.resize(0,500);
  image(insta, 800, 30);


  //taskbar
  noStroke();
  fill(170);
  rect(0,windowHeight-28, windowWidth, 70);

  //first tab
  fill(240);
  rect(71,windowHeight-25,200,21);

  fill(50);
  rect(72,windowHeight-23,202,22);

  fill(170);
  rect(72,windowHeight-24,200,21);

  fill(0);
  textSize(11);
  text('Notepad - [Untitled]', 100,windowHeight-8);

  note.resize(20,0);
  image(note, 75, windowHeight-24);

  //second tab
  fill(240);
  rect(279,windowHeight-25,200,21);

  fill(50);
  rect(280,windowHeight-23,202,22);

  fill(170);
  rect(280,windowHeight-24,200,21);

  fill(0);
  textSize(11);
  text('Instagram.exe', 308,windowHeight-8);

  insta1.resize(35,0);
  image(insta1, 275, windowHeight-26);


  //third tab

  fill(240);
  rect(488,windowHeight-22,203,21);

  fill(50);
  rect(486,windowHeight-25,203,22);

  fill(200);
  rect(488,windowHeight-23,202,21);

  game1.resize(24,0);
  image(game1,488,windowHeight-25);

  fill(0);
  textSize(11);
  text('4-card monte', 517,windowHeight-7);
  text('4-card monte', 516,windowHeight-7);


  //The icons
  trash.resize(40,0);
  image(trash,windowWidth-70,windowHeight-100);

  fill(255);
  textSize(10);
  text('Recycle Bin', windowWidth-81,windowHeight-40);

  computer.resize(40,0);
  image(computer,33,50);
  text('My Computer',15,110);

  docs.resize(50,0);
  image(docs,26,130);
  text('My Documents', 10,190);

  web.resize(110,0);
  image(web,-7,200);
  text('Internet', 28,265);
  text('Explorer', 28,283);

  note1.resize(43,0);
  image(note1,26,310);
  text('Notepad', 26, 373);

  game.resize(55,0);
  image(game,22,390);
  text('4-card monte', 12,460);

  start.resize(60,0);
  image(start,7,windowHeight-26);




  //The clock
  //Inspired by https://editor.p5js.org/D_Snyder/sketches/Xtx2Zu9D5?fbclid=IwAR354EK6enZaiSsPIfBINhD-K0VIlMaIaQLlUDXYNAgol8u9SN79HofXCdY
  fill(0);
  textSize(12);
  let Hour = hour();
  let min = minute();
  let noon = Hour >= 12? " PM" : " AM"
  if(min < 10)
  min = "0"+min
  Hour%=12
  text(Hour+":"+min+noon, windowWidth-75, windowHeight-8);


if(wel == 0 || wel == 9){
    welcome()
  }else if(wel == 1 && test == 1){
    round1()
  }else if(wel == 2){
    r1c1();
  }else if(wel == 3){
    r1c2();
  }else if(wel == 4){
    r1c3();
  }else if(wel == 5){
    r1c4();
  }else if(wel == 6){
    round2();
  }else if(wel == 7){
    r2c1();
    virus();
  }else if(wel == 8){
    r2c4();
    virus();
  }else if(wel == 9){
    restart();
  }



  //Instagram metrics
  if (wel == 7 || wel == 8){
    posts = posts + postsdown;
    followers = followers + followersdown;
    following = following + followingdown;

      if(posts == 0){
        postsdown = 0;
      }

      if(followers == 0){
        followersdown = 0;
      }

      if(following == 0){
        followingdown = 0;
      }

  } else {
    posts = 157;
    followers = 6420;
    following = 437;
  }
  push();
  textSize(9.5);
  textAlign(CENTER);
  text(posts, 1097,132);
  text(followers,1149,132);
  text(following,1206,132);
  pop();

if (test == 0){
  fill (190);
  rect(1001.5,272,82.5,78);
    rect(1085,272,82,78);
    rect(1168,272,80,78);
    rect(1000.5,352,84,78);
    rect(1085, 352, 82,79);
    rect(1168,352,82,78);
    rect(1001, 432, 83, 62);
    rect(1085, 432, 82, 62);
    rect(1168, 431, 81, 63);
    ellipse(1035,150,60,60);
}

  //removing pictures by placing grey rectangles on top of them
  fill(190);

  //pic 1 (from top left to right)
  //from https://www.tumgir.com/tag/Pixel%20background
  if(posts <= 1){
  rect(1001.5,272,82.5,78);
  }

  //pic 2
  //from https://i.pinimg.com/736x/24/c8/b2/24c8b271247619f2743c33cb75e464d9--goodvibes-quotes-inspiring-quotes.jpg
  if(posts <= 2){
  rect(1085,272,82,78);
  }

  //pic 3
  //from https://66.media.tumblr.com/cc0f64e95a24b09083789d55627b49d4/tumblr_p6w06uZuDQ1v1j4s4o1_1280.png
  if(posts <= 3){
  rect(1168,272,80,78);
  }

  //pic 4
  //from https://data.whicdn.com/images/330762924/superthumb.jpg?t=1558747976
  if(posts <= 4){
  rect(1000.5,352,84,78);
  }

  //pic 5
  //from http://www.imgrum.net/tag/hotlinemiami
  if(posts <= 5){
  rect(1085, 352, 82,79);
  }

  //pic 6
  //from https://www.pinterest.com.au/pin/644437027916186376/
  if(posts <= 6){
  rect(1168,352,82,78);
  }

  //pic 7
  //from https://dribbble.com/shots/9326429-Nike-Next-Ekiden
  if(posts <=7){
  rect(1001, 432, 83, 62);
  }

  //pic 8
  //from https://theinstaprofile.com/share/B5wb63AAXKH
  if(posts <= 8){
  rect(1085, 432, 82, 62);
  }

  //pic 9
  //from https://wallpaperplay.com/board/pool-water-wallpapers
  if(posts <= 9){
  rect(1168, 431, 81, 63);
  }

if(test == 0){


  fill(185);
  push();
  strokeWeight(0.5);
  stroke(50);
  rectMode(CENTER);
  rect(windowWidth/2+150,windowHeight/2,300,200);
  fill(2,0,120);
  rect(windowWidth/2+150,windowHeight/2-88,293,17);
  fill(185);
  rect(windowWidth/2+137+150,windowHeight/2-88,14,14);
  fill(85);
  textSize(12);
  text('x', windowWidth/2+134+150,windowHeight/2-84);
  pop();

  textSize(13);
  fill(0);
  text('Write your Instagram username', windowWidth/2-120+150,windowHeight/2-20);

//}

}

  //The cursor
  if (wel == 7 || wel == 8) { //if virus is activated, the cursor moves on its own
    pointer.resize(20,0);
    image(pointer,cursorx,cursory);
    cursorx = cursorstartx;
    cursory = cursorstarty;


    if(cursorstartx > width || cursorstartx < 0) {
      xspeed = xspeed * -1;
    }

    if(cursorstarty > height || cursorstarty < 0) {
      yspeed = yspeed * -1;
    }
    cursorstartx = cursorstartx + xspeed + random(-2,2);
    cursorstarty = cursorstarty - yspeed - random(-3,3);

  } else {
    cursorx = mouseX;
    cursory = mouseY;
    pointer.resize(20,0);
    image(pointer,cursorx,cursory);
  }

  //
  if (wel == 8 && bbb == 900) {
    window.location.reload();
  }

  if (test == 1) {
    var name = input.value();
    fill(0);
    textSize(10);
    text(name,1115,105);
    text(name,1114,105);
    text(name, 1010,200);
    text(name, 1011,200);
    textSize(8);
    text('Ars Electronica 2020',1010,217);
    input.hide();
    button.hide();

  }
}



function mousePressed(){
  if (test == 1){
  if(wel == 9 && test == 1 && mouseX > 145 && mouseX < 778 && mouseY > 100 && mouseY < 445){ //inside the game window
    wel = 1
  } else if(wel == 1 && mouseX > 190 && mouseX < 290 && mouseY > 200 && mouseY < 400){ //card 1
    wel = 2
  } else if(wel == 1 && mouseX > 340 && mouseX < 440 && mouseY > 200 && mouseY < 400){ //card 2
    wel = 3
  } else if(wel == 1 && mouseX > 490 && mouseX < 590 && mouseY > 200 && mouseY < 400){ //card 3
    wel = 4
  } else if(wel == 1 && mouseX > 640 && mouseX < 740 && mouseY > 200 && mouseY < 400){ //card 4
    wel = 5
  }else if(wel == 2 || wel == 3 || wel == 4 || wel == 5 && mouseX > 145 && mouseX < 778 && mouseY > 100 && mouseY < 445){ //inside game window after first pick
    wel = 6
  }else if(wel == 6 && mouseX > 190 && mouseX < 290 && mouseY > 200 && mouseY < 400){ //card 1 round 2
    wel = 7
  } else if(wel == 6 && mouseX > 640 && mouseX < 740 && mouseY > 200 && mouseY < 400){ //card 4 round 2
    wel = 8
  }
}
}
function welcome(){ //the welcome screen before entering the game
  push();
  textAlign(CENTER);
  fill(150,0,0);
  textSize(15);
  text('4-card monte',461, 140);

  fill(0);
  textSize(10);
  text('This is a card game.',461, 200);
  text('You have to find the red card to win',461,220);
  text('If you pick a black card, the computer wins',461,240);
  text('There are 2 red cards and 2 black cards',461,260);
  text('So you have an even 50/50 chance of winning!',461,280);
  text('Try and see if you can beat the computer...',461,300);
  text('Keep in mind, fortune favours the bold',461,320);
  text('Press anywhere to start',461,380);
  pop();
}

function round1(){ //first round after initiating game
  push();
  textAlign(CENTER);
  text('Pick a card!',461,140);

  textSize(9);
  text('You: 0      Me: 0', 715,435);
  pop();

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
}

function r1c1(){  //First card picked - round one
  push();
  textAlign(CENTER);
  text('Great!',461,140);
  text('Press anywhere to continue',461,400);

  textSize(9);
  text('You: 1      Me: 0', 715,435);

  redcard.resize(100,0);
  image(redcard, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
  pop();
}

function r1c2(){  //Second card picked - round one
  push();
  textAlign(CENTER);
  text("Excellent!",461,140);
  text('Press anywhere to continue',461,400);
  textSize(9);
  text('You: 1      Me: 0', 715,435);

  cardback.resize(100,0);
  image(cardback, 190, 200);

  redcard.resize(100,0);
  image(redcard, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
  pop();
}

function r1c3(){  //Third card picked - round one
  push();
  textAlign(CENTER);
  text('Well done!',461,140);
  text('Press anywhere to continue',461,400);
  textSize(9);
  text('You: 1      Me: 0', 715,435);

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  redcard.resize(100,0);
  image(redcard, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
  pop();
}

function r1c4(){  //Fourth card picked - round one
  push();
  textAlign(CENTER);
  text('Good job!',461,140);
  text('Press anywhere to continue',461,400);
  textSize(9);
  text('You: 1      Me: 0', 715,435);

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  redcard.resize(100,0);
  image(redcard, 640, 200);
  pop();
}

function round2(){
  push();
  textAlign(CENTER);
  text('Pick a card!',461,140);

  textSize(9);
  text('You: 1      Me: 0', 715,435);
  pop();

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
}

function r2c1(){  //First card picked - round two
  push();
  textAlign(CENTER);
  text('Game over!',461,140);

  textSize(9);
  text('You: 1      Me: 1', 715,435);
  pop();

  blackcard.resize(100,0);
  image(blackcard, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
}


function r2c2(){  //Second card picked - round two (Nothing happens)
  push();
  textAlign(CENTER);
  textSize(9);
  text('You: 1      Me: 0', 715,435);
  pop();

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
}

function r2c3(){  //Third card picked - round two (Nothing happens)
  push();
  textAlign(CENTER);
  textSize(9);
  text('You: 1      Me: 0', 715,435);
  pop();

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  cardback.resize(100,0);
  image(cardback, 640, 200);
}

function r2c4(){  //Fourth card picked - round two
  push();
  textAlign(CENTER);
  text('Game over!',461,140);
  textSize(9);
  text('You: 1      Me: 1', 715,435);
  pop();

  cardback.resize(100,0);
  image(cardback, 190, 200);

  cardback.resize(100,0);
  image(cardback, 340, 200);

  cardback.resize(100,0);
  image(cardback, 490, 200);

  blackcard.resize(100,0);
  image(blackcard, 640, 200);
}

function virus(){
  textSize(9);
  if(posts == 0){
    bbb++

    if(bbb >= 0){
      var name = input.value();
      text("Hahahahahah " + name + " you have been hacked!", 415,555);
    }

    if(bbb >= 70){
      text("How does it feel not being in control?",415,575);
    }

    if(bbb >= 140){
      text("You might as well get used to it",415,595);
    }
    if(bbb >= 280){
      text("Nothing is beyond my reach",415,615);
    }
  }
}
