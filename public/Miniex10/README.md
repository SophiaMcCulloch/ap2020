<h1>Flowcharts</h1>

Individual flowchart:

![Screenshot](flow.png)

For my individual flowchart I have chosen to make a flowchart of my miniex 7, the generative program. For my miniex 7 I made a Perlin Noise flowfield with blue/green lines. The program was quite complicated and there were a lot of different components, and I had the impression also from my feedback, that the program was a little difficult to understand. 
For my flowchart I have therefor decided to make it as simple as possible while still going through the main components of the program. I have tried to make the language in the flowchart as far from syntax as I could, however I felt it would become a little long winded to explain how vectors work in the flowchart. Furthermore, vectors are also used in mathematics and physics so it is not only a concept used in coding.
As it is a generative program there is no “end”, and this is why I have shown a loop at the end of my flowchart, to visualise that the program keeps on running as long as you let it. 
I think that this flowchart helps visualise the processes of my program, and I can see how flowcharts can be a useful tool for communicating how a program works. 


Group flowcharts:

![Screenshot](flow2.png)
For the group work we have created two flowcharts to visualise two different ideas. The first flowchart visualises an idea of a game that you cannot win, where you have to turn over some cards where you are informed that your odds are 50/50, however in reality you will always loose in the end. After you have lost, you are informed that you now have a virus, and the cursor begins to move my itself. This is to touch upon some aspects of control and also randomness in computing. A technical challenge in this idea could be how to ensure that the player always loses. Here we talked about that we wanted it to be apparent in the code that the chances actually were 50/50 to begin with, but as you click on a card it changes so you lose. 

The other flowchart describes a program where webcam capture is used with the ascii library to turn the webcam capture into ascii art. Furthermore, we wanted to create a .json file with text about data and data capturing which should “rain” down from the top of the canvas. This program was supposed to touch on the subject “capture all” where you see video of yourself turned into symbols, to visualise that everything you to online is data that can be captured and used. 

For my individual flowchart I have focused on how the program technically is able to execute, and how the algorithmic procedures are to be understood. For the group flowcharts we have focused more on the conceptual aspect of the program and which elements we would like to include, without thinking about how these functions would technically be carried out. The reason for this being the purpose of the flowcharts. For my individual flowchart I have already written the code, and this is what I am trying to explain with my flowchart. For the group flowchart we have not yet written the programs, and here the flowchart’s purpose is more to communicate different ideas and visualise them in steps as a part of the design process. We have not yet decided on an idea yet, but visualising some ideas as flowcharts has helped us think further about the possibilities of these ideas. This is also something that Nathan Ensmenger mentions in The Multiple Meanings of a Flowchart (2016): “(…) the chart functions largely as a design technology, a ‘thing for thinking with,’”

It is therefore important when making a flowchart to make clear what you want the function of this flowchart to be, of it is to explain the algorithmic procedures, or if it is to communicate an idea or concept, as the flowchart “Serves multiple services simultaniously ”  (Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.", 2016)
