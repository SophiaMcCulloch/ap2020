![Screenshot](cat.png)

https://sophiamcculloch.gitlab.io/ap2020/Miniex9/

<h1>Miniex 9
Que(e)ry Data</h1>

For this week’s miniex we have created what looks like the Google search engine with the question “What is happiness” already typed into the search box. When you press the search button, a random image or gif of a cat appears.
To display the image of a cat we have used an API called The cat API where you can retrieve random cat images and animated gifs.
 
Our thoughts behind this program was to show the selection of data that goes on when we use for example the Google search engine. You can say that Google in many ways has a monopoly on the truth, as they decide which results are the first to appear and therefore also the results and answers we click on and read. Another example of this is when you ask a home assistant like the Amazon Alexa a question, it only offers one answer. This is also what we have tried to portray through our program, as the question “What is happiness” is a very broad question and not something that has a definitive answer. However, by showing only one type of answer we are trying to comment on the problems that are connected with this sort of data selection. How is the various data prioritized? It is very important to remember this aspect. These big corporations chose whatever algorithm/code to determine what the answer should be to your request, but who says this is the definitive correct answer to present? This also means that what we see and what we are exposed to essentially is motivated by corporate interests. We find a problem in this, because we tend to believe what we are presented with, and this is the particular issue we wanted to comment on. It’s an attempt to break with the idea that we can just trust the answers and the “truths” we are familiarized with.
 
In the beginning we had our eye on another API to include in our program, which was an API that retrieved random steps from WikiHow. This would’ve made great sense, since the answer to “how to achieve happiness” would’ve been a 3-step guide, randomly picked from miscellaneous “how-to-guides”,  which wouldn’t make sense in answering the question and thus underlined our aforementioned point of the program. However, we had a lot of difficulty getting this API to work, and unfortunately we had to give up on it in the end. Through the process of creating this program we have learned a lot about APIs and especially how challenging it can be to figure out how the different APIs work, and that the security surrounding these APIs also vary a lot depending on what kind of data you are accessing. An example of this we learned was the Twitter API which we also checked out, and the application process for getting your API-key for this is very extensive and I have yet to receive mine.

By group 6:
Jonas, Mads, Heinrich and Sophia 