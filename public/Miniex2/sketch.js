function setup() {
  createCanvas(500, 400);
}

function draw() {
  background(229, 194, 255);

  //the pills
  //the rectangle
  strokeWeight(1);
  stroke(10);
  fill(200);
  rect(50, 50, 100, 235, 10);

  //pills to the left
  fill(255);
  circle(70, 68, 15);
  circle(70, 90, 15);
  circle(70, 112, 15);
  circle(70, 133.5, 15);
  circle(70, 155, 15);
  circle(70, 176.5, 15);
  circle(70, 198, 15);
  circle(70, 219.5, 15);
  circle(70, 241, 15);
  circle(70, 263, 15);

  //pills to the right
  circle(130, 68, 15);
  circle(130, 90, 15);
  circle(130, 112, 15);
  circle(130, 133.5, 15);
  circle(130, 155, 15);
  circle(130, 176.5, 15);
  circle(130, 198, 15);
  circle(130, 219.5, 15);
  circle(130, 241, 15);
  circle(130, 263, 15);

  //pill in the middle
  circle(100, 68, 15);

  //line down the middle
  stroke(220);
  strokeWeight(3);
  line(100, 90, 100, 240);


  //the condom

  //the base
  strokeWeight(20);
  stroke(180, 183, 217);
  fill(200, 205, 247);
  ellipse(336, 198, 170, 100);

  //shapes to cover the outline of the base in the tip
  noStroke();
  ellipse(324, 153, 30, 60);
  ellipse(348, 153, 30, 60);
  rect(313, 135, 45, 30);

  //the tip
  noFill();
  stroke(180, 183, 217);
  strokeWeight(5);
  curve(220, 232, 295, 188, 311, 139, 311, 92);
  curve(365, 111, 360, 135, 378, 188, 447, 234);
  fill(200, 205, 247);
  bezier(311, 139, 315, 103, 354, 99, 360, 135);
  fill(255);
  noStroke();
  ellipse(327, 140, 10, 20);
}
