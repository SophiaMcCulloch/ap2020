let button;
let ctracker;
let myFont;
let trash;
let computer;
let docs;
let web;
let start;
let pointer;

function preload() {
  myFont = loadFont('assets/fontt.ttf');
  trash = loadImage('assets/trash.png');
  computer = loadImage('assets/computer.png');
  docs = loadImage('assets/docs.png');
  web = loadImage('assets/web.png');
  start = loadImage('assets/start1.png');
  pointer = loadImage('assets/pointer.png');
}


function setup() {
  background(100);

  //web cam capture
  let capture = createCapture(VIDEO);
  capture.size(600,400);
  capture.position(0,0);
  //capture.hide();
  let c = createCanvas(640, 480);
  c.position(0,0);

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //styling the buttons with CSS
  button = createButton('OK');
  button.position(470,300);
  button.style("padding","4px");
  button.style("color","#00000");
  button.style("text-shadow","1px 1px rgb(255,255,255)");
  button.style("outline","none");

  button1 = createButton('Cancel');
  button1.position(500,300);
  button1.style("padding","4px");
  button1.style("color","#818181");
  button1.style("text-shadow","1px 1px rgb(255,255,255)");
  button1.style("outline","none");

  noCursor();
}

function draw() {

clear();
  let positions = ctracker.getCurrentPosition();

//to create the blue background and floating head
fill(41, 69, 227);
noStroke();
if (positions.length) {
  console.log("hej");
beginShape();
vertex(0,0);
vertex(width,0);
vertex(width,height);
vertex(0,height);
beginContour();

for (var i=0; i<15; i++){
  vertex(positions[i][0], positions [i][1]);
  }
vertex(positions[15] [0], positions [15] [1]);
vertex(positions[16] [0], positions [16] [1]);
vertex(positions[17] [0], positions [17] [1]);
vertex(positions[18] [0], positions [18] [1]);
vertex(positions[22] [0], positions [22] [1]);
vertex(positions[21] [0], positions [21] [1]);
vertex(positions[20] [0], positions [20] [1]);
vertex(positions[19] [0], positions [19] [1]);
endContour();
endShape(CLOSE);
}

//background text
fill(41, 69, 227);
textSize(20);
textFont(myFont);
text('You cant hide', 220, 100);
text('You cant hide', 50, 200);
text('You cant hide', 300, 40);
text('You cant hide', 260, 300);
text('You cant hide', 60, 400);

//info windows
box(440,140);
box(450,150);
box(460,160);

box(470,170);
box(460,180);
box(450,190);
box(440,200);
box(430,210);

//The icons
image(trash,570,420);
trash.resize(40,0);

image(computer,20,50);
computer.resize(40,0);

image(docs,12,110);
docs.resize(50,0);

image(web,-20,170);
web.resize(110,0);

image(start,0,452);
start.resize(70,0);

//The cursor
image(pointer,mouseX,mouseY);
pointer.resize(20,0);

//the information window 
function box(x,y) {
//large rectangle
fill(191);
rect(x,y,150,130);

//rectangle header
fill(105);
rect(x+3,y+3,143,20);

//crossbutton
fill(191);
stroke(0);
rect(x+128,y+6,14,14);
fill(0);
textSize(12);
text('x', x+132,y+17);

//text on box
textSize(10);
text('We know everything', x+12,y+68);
}
}
