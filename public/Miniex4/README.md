![Screenshot](billede.png)

https://sophiamcculloch.gitlab.io/ap2020/Miniex4/


<h1>“We know everything”</h1>


For this week’s mini ex I have created a desktop inspired by Windows98 with a facetracker that displays only the face captured by webcam, and infowindows 
with the text: “We know everything” and as you move your face around you can see the text “You can’t hide” hidden in the background. 
For the facetracker I downloaded clmtracker and then covered the whole window with a blue rectangle. Then I used beginContour to cut the 
face out using the clmtracker points around the face as vertexes using a for-loop. 
I have used function preload to load all of the small icons for the desktop including the cursor and the font. I created my own function for 
the information window with the x and y positions as variables to enable me to create multiple windows that I could stack on top of each other. 
I also included two DOM element buttons which I styled with CSS, however nothing happens when you press them. 

The two buttons are also meant to remind you a little bit of the “accept cookies” button on websites, where you in reality do not really have a choice, it is more of an information. Søren Pold talks about buttons in his article “Button”, where he argues that buttons force decisions into binary choices and that there is no way to partially agree, or declare that you do not know the consequences of pressing the button. This is also true in my miniex, where there are only two options, an ‘OK’ button and a ‘Cancel’ button. However the cancel button is grey, indicating that it is inactive or not available. Although when you press either of the buttons nothing happens. This is to convey the feeling that you are not in control, for as Søren Pold writes in Button: “A button indicates a functional control; something well defined and predictable will happen as a result of the user pressing it” (Pold, Søren: “Button”, Software Studies Lexicon, p. 31). Here there does not happen anything well defined and predictive, nothing happens and this creates the feeling of not being in control. 

During the creation of this miniex I have learned how to use function preload to load pictures and fonts into my runme, and how to scale them down/up in size. I have also learned how to implement other libraries into Atom to use in my runme, and specifically I have learned how to use the clmtracker. Lastly I have learned to use the beginContour function for creating negative space in shapes. 

By creating a desktop that is reminiscent of the Windows98 interface it reminds us of a time where we did not have all the concerns regarding our online life and data capture that we have today. Although it is not that long ago computer interfaces looked like this, the technological development has moved fast since then, and it is sometimes hard to fathom how much of our daily lives are dependent on these devices and today’s technology. However, with almost every aspect of our lives being tracked and captured by computers, we are in a situation today that is relatively new to all of us, and many have trouble comprehending to what extent this affects us, and how worried we should be. By including the facial capture I wanted to create the feeling that the computer is always watching you, and when you move your face around the text “you can’t hide” appears, emphasising this feeling. It is a scary thought that all of our data is being captured, and this is also the feeling that I am trying to convey in my miniex. It is the feeling of buying a small cute lizard and then realising that it grew up to be a huge crocodile and now you don’t know what to do with it. Technology back then seemed harmless and innocent, and I don’t think many of us could have foreseen how it would evolve.   
