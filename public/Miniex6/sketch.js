//miniex6
let dragon;
let mic;
var pipes = [];
var score = 0;

function preload() {
  dragon = createImg('dragon.gif');
  }

function setup() {
  createCanvas(800, 500);
  frameRate(40);

  pipes.push(new Pipe());
  dragon1 = new Dragon();

  // audio capture
  mic = new p5.AudioIn();
  mic.start();

  }

function draw() {
  background(0);

  fill(200, 0, 0);
  textSize(32);
  text(score/2, 20, 50);

  dragon1.show();
  dragon1.move();

//the pipes
  for (var i = pipes.length - 1; i >= 0; i--) {
    pipes[i].show();
    pipes[i].update();

    if (pipes[i].hits(dragon1)) {
         console.log('HIT');
         score = 0;
       }else if(frameCount%113 == 0){
      score = score + 10;
    }

    if (pipes[i].offscreen()) {
      pipes.splice(i, 1);
    }
  }

  if (frameCount % 75 == 0) {
    pipes.push(new Pipe());
  }
}
