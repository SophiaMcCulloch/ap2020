![Screenshot](bat.png)

https://sophiamcculloch.gitlab.io/ap2020/Miniex6/

Miniex 6

For my miniex 6 I have created a game much like flappy bird, inspired by Daniel Schiffman’s coding challenge#31. My approach to this miniex has been different from the previous ones as this time I have chosen to base my program on an already existing program instead of creating it from scratch. The ‘main character’ in the game is a small dragon which has to avoid the white rectangles. You make the bat move up and down the y-axis using your voice. This I have done by creating an audio capture where I have mapped the amplitude to the  dragon’s y position. I thought that this might be fun because you normally sit quietly and play games, but here it would be impossible to play this game out in public without being stared at.
I created a class for the dragon which I made in a separate js file and I also made a separate file for the rectangles so I was able to keep my sketch.js file as tidy as possible. 

For the dragon object I have a show() function and a move() function. In the show function I had the animated gif and the size of the dragon. And in the move function I mapped the dragon’s y-position to the amplitude of the audio input, and I also specified that the dragon should not be able to move out of the canvas size. 

I also created an object for the rectangles where I used this in an array where every 75 frames a new rectangle appears. This I did by writing that if frameCount % 75 == 0 there should appear a new rectangle (pipe). 
I also made a function for checking if the dragon hits the rectangle, and this I used to make the rectangle change colour to red if it is hit. I also used it to reset the score, however the score part did not quite work out for me how I wanted, as I would like the score to go up by 10 every time the dragon passed through the gaps between the rectangles, however this I struggled a lot with.  

Object oriented programming have four pillars: Encapsulation, Abstraction, Inheritance and Polymorphism. One of the most important concepts is abstraction. Abstraction is about selecting only the most important aspects of a larger pool of data to show in order to reduce complexity. However, the choices about which abstractions to make has a large impact on the final product and how it is perceived, and this also goes to show that programming is not a neutral act. An example of this could be video games where many female characters are depicted as very sexualised and without much depth. These are abstractions that are made at a programming level, and these choices have a wider cultural impact.  
