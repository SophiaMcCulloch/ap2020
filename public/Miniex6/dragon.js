class Dragon {

  constructor() {
   this.x = 15;
   this.y = 430;
  }

  show() {
    imageMode(CENTER);
    image(dragon,this.x,this.y);
    dragon.size(70,70);
    dragon.position(this.x,this.y);
}

  move() {
    //get the amplitude
    this.vol = mic.getLevel();

    //map the sound level to the position of the dragon
    this.pos = map(this.vol, 0, 0.6, 430, 0);
    this.y = this.pos;

      //reset if the dragon moves out of range
      if (this.y > 500) {
        this.y  = 500;
      }
}
}
