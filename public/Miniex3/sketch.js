function setup() {
  createCanvas(500, 500);
}

function draw() {
  background(237, 194, 252);
  rectMode(CENTER);
  translate(150, 150);

  //to make the snail rotate
  translate(p5.Vector.fromAngle(millis() / 800, 60));
  snail();
  }

function snail() {
  //eyes
  stroke(125, 125, 87);
  line(72,98,70,75);
  line(74,98,79,73);
  fill(255);
  stroke(0);
  ellipse(70,75,5);
  ellipse(79,73,5);
  ellipse(70,75,1);
  ellipse(79,73,1);


  //body
  fill(255, 226, 122);
  stroke(209, 193, 136);
  rect(100, 100, 60, 10,10);

  //snailhouse
  fill(157, 227, 168);
  stroke(120, 173, 125);
  ellipse(115,88,40);
}
