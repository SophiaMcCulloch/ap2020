![Screenshot](Skærmbillede_2020-02-21_kl._13.33.46.png)

https://sophiamcculloch.gitlab.io/ap2020/Miniex3/

I have made a spinning snail throbber that is comprised of a rectangle for its body, lines and ellipses as eyes and an ellipse for the snail house. I made my snail into a function to enable it to rotate. I also wanted to make a spiral on the snail house, however I had a lot of trouble with this, so I ended up giving up on that.
I have also used the syntax millis(); which returns the number of milliseconds since the program started. This I have used with the translate (); syntax with a vector make the snail move in a circle. 

For my miniex this week I have focused more on learning the new syntax than creating a very expressive throbber, so the only considerations I have made about my throbber is that I made a snail because sometimes experiencing throbbers can challenge our patience, and the snail is a notoriously slow animal. 

A throbber communicates that something is loading and in process. Therefor you can say that a throbber is a metaphor for what is happening inside the computer. As Soon writes in her article "Throbber: Executing Micro-temporal Streams" "the throbber icon acts as an interface between computational processes and visual communication".
Time can be said to be a social construct in the way that it is something we humans have created systems of meaning for that creates the concept of time. A computer has no time perception, and throbbers are solely made for our purpose, to communicate to us that we have to wait for a while. 
