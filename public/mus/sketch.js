let p1 = false;
let p2 = false;
let p3 = false;
let p4 = false;
let lol;


function preload() {
  gulv = loadImage('assets/gulv.png');
  billede1 = loadImage('assets/stat.jpg');
  billede2 = loadImage('assets/jonas.jpg');
  myFont = loadFont('assets/font.ttf');
  billede3 = loadImage('assets/billede3.png');
  billede31 = loadImage('assets/billede31.png');
  chili2 = loadImage('assets/chili2.jpg');
  chili1 = loadImage('assets/chili.jpg');
  lol = loadModel('assets/man.obj', true);
  helvetica = loadFont('assets/Helvetica1.ttf');
  helveticab = loadFont('assets/HelveticaB.ttf');
  sound = loadSound('assets/museum.mp3');

}
function setup() {
  createCanvas(1500, 900, WEBGL);
  sound.loop();
}

function draw() {
  translate(-750,-450);
  background(202, 219, 219);
  noStroke();

  //gulv
  image(gulv,0,170);
  gulv.resize(1700,0);


  //venstre væg
  fill(122, 27, 27);
  beginShape();
  vertex(0, 0);
  vertex(0, 900);
  vertex(350, 400);
  vertex(350, 0);
  endShape(CLOSE);

  //væg midte
  fill(166, 36, 36);
  beginShape();
  vertex(350, 0);
  vertex(350, 400);
  vertex(1500, 400);
  vertex(1500, 0);
  endShape(CLOSE);


  //montre
  //foran
  fill(115, 115, 115);
  rect(800,500,100,200);

  //til venstre
  fill(59, 59, 59);
  beginShape();
  vertex(738, 479);
  vertex(800, 500);
  vertex(800, 700);
  vertex(738, 678);
  endShape(CLOSE);

  //ovenpå
  fill(240, 235, 235);
  beginShape();
  vertex(738, 479);
  vertex(840, 479);
  vertex(900, 500);
  vertex(800, 500);
  endShape(CLOSE);

  push();
  translate(130,550);
  rotate(PI / 0.577);
  textFont(helveticab);
  textSize(12);
  text('ESC_room',0,0);
  textFont(helvetica);
  text('Trip to Paris',80,0);
  pop();

  // statuebillede
  textFont(helveticab);
  textSize(12);
  text('ESC_room',450,320);
  textSize(14);
  textFont(helvetica);
  text('Museum visit',524,320);

  image(billede1,450,50);
  billede1.resize(250,0);



  //interaktion med statuebillede
  if (p1 == true){
    first();
  }

  //chilibillede lille
  fill(255);

  textFont(helveticab);
  textSize(12);
  text('ESC_room',900,325);
  textSize(14);
  textFont(helvetica);
  text('Nature is beautiful',974,325);

  image(chili2,900,50);
  chili2.resize(259,0);


  //interaktion med chilibillede
  if (p4 == true){
    fourth();
  }

  //montrebillede
  textFont(helveticab);
  textSize(10);
  text('ESC_room',803,515);
  textSize(12);
  textFont(helvetica);
  text('Me',865,515);

  //interaktion med montrebillede
  if (p2 == false){

    image(billede2,755,360);
    billede2.resize(130,0);

  }

  if (p2 == true){
    second();
  }

  //billede til venstre
  image(billede3,120,-130);
  billede3.resize(190,0);


  //interaktion med billede 31
  if (p3 == true){
    third();
  }

  function first(){
    fill(0);
    rect(450,50,250,250);
    fill(40, 214, 34);
    textFont(myFont);
    textSize(9);
    text('let p1 = false;', 460,70);
    text('function preload() {', 460,85);
    text('  billede1 = loadImage(assets/stat.jpg);', 460,100);
    text('}', 460,115);
    text('function draw() {', 460,130);
    text('  image (billede1,450,50);', 460,145);
    text('  billede1.resize(250,0);', 460,160);
    text('  if (p1 == true){', 460,175);
    text('  first();', 460,190);
    text('}', 460,205);
    text('function mousePressed(){', 460,220);
    text('  if (p1==false&&mouseX>450&&mouseY>52...', 460,235);
    text('  p1 = true;', 460,250);
    text('} else if (p1==true&&mouseX>450&&mouseY..', 460,265);
    text('  p1 = false;', 460,280);
    text('}', 460,295);
  }

  function second(){
push();
    pointLight(153, 247, 255, -50, 0, 500);

    translate(800,380,100);
    noStroke();

    rotateY(frameCount * 0.01);
    rotateX(3.1);

    ambientMaterial(255,255,255);

    model(lol);
    pop();
  }

  function third(){

    image(billede31,120,-125);
    billede31.resize(190,663);
  }

  function fourth(){

    //stor chili
    image(chili1,808,12);
    chili1.resize(500,0);
  }

}

function mousePressed(){
  if (p1==false && mouseX > 450 && mouseY > 52 && mouseX < 700 && mouseY< 300){
    p1 = true;
  } else if (p1==true  && mouseX > 450 && mouseY > 52 && mouseX < 700 && mouseY < 300){
    p1 = false;
  } else if (p2==false && mouseX > 756 && mouseY > 363 && mouseX < 886 && mouseY < 490){
    p2 = true;
  }else if (p2==true && mouseX > 756 && mouseY > 262 && mouseX < 886 && mouseY < 490){
    p2 = false;
  }else if (p3==false && mouseX>100 && mouseY>50 && mouseX<300 && mouseY<500){
    p3 = true;
  }else if (p3==true && mouseX>100 && mouseY>50 && mouseX<300 && mouseY<500){
    p3 = false;
  }else if (p4==false && mouseX>900 && mouseY>50 && mouseX<1155 && mouseY<305){
    p4 = true;
  }else if (p4==true && mouseX>805 && mouseY>10 && mouseX<1305 && mouseY<388){
    p4 = false;
  }


  //console.log(p1);
  //console.log(p2);
  //console.log(mouseY);
}
