class Particle {

  constructor() {
  //placing the paticle at a random position
  this.pos = createVector(random(width), random(height));

  //the velocity
  this.vel = createVector(0, 0);

  //the acceleration
  this.acc = createVector(0, 0);

  //maximum speed of the particles
  this.maxspeed = 4;

  //the b value for the colour (rgb)
  this.b = 0;

  this.prevPos = this.pos.copy();
}

  update() {
    //adding some of the physics aspects to create the right movement
    this.vel.add(this.acc);
    this.vel.limit(this.maxspeed);
    this.pos.add(this.vel);
    //reset the acceleration to 0, by multiplying the vector by 0
    this.acc.mult(0);
  }

//to make the particles follow the vectors
  follow(vectors) {
    var x = floor(this.pos.x / scl);
    var y = floor(this.pos.y / scl);
    var index = x + y * cols;
    var force = vectors[index];
    //add the force to the acceleration
    this.acc.add(force);
  };

  show() {
    //The colour of the particles
    stroke(93, 232, this.b, 25);

    //incrementing this.b to make the colour change
    this.b = this.b + 1;
    if (this.b >245) {
      this.b = 90;
    }
    strokeWeight(1);

    //the line being drawn
    line(this.pos.x, this.pos.y, this.prevPos.x, this.prevPos.y);
    this.updatePrev();
  };

  updatePrev() {
    //the x and y of the line
    this.prevPos.x = this.pos.x;
    this.prevPos.y = this.pos.y;
  };

//To make the particles wrap around the edges of the canvas
  edges() {
    if (this.pos.x > width) {
      this.pos.x = 0;
      this.updatePrev();
    }
    if (this.pos.x < 0) {
      this.pos.x = width;
      this.updatePrev();
    }
    if (this.pos.y > height) {
      this.pos.y = 0;
      this.updatePrev();
    }
    if (this.pos.y < 0) {
      this.pos.y = height;
      this.updatePrev();
    }
  };
}
