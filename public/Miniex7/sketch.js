var inc = 0.1;
var scl = 10;
var cols, rows;

var zoff = 0;

var particles = [];

var flowfield;

function setup() {
  createCanvas(windowWidth, windowHeight);

  //creating a grid across the canvas
  cols = floor(width / scl);
  rows = floor(height / scl);

  flowfield = new Array(cols * rows);

//For-loop placing 300 particles in an array
  for (var i = 0; i < 300; i++) {
    particles[i] = new Particle();
  }

  background(255);
}

function draw() {

  //calculating the vectors using perlin noise
  var yoff = 0;
  for (var y = 0; y < rows; y++) {
    var xoff = 0;
    for (var x = 0; x < cols; x++) {
      var index = x + y * cols;
      var angle = noise(xoff, yoff, zoff) * TWO_PI * 4;
      var v = p5.Vector.fromAngle(angle);
      v.setMag(1);

      //storing all the calculated vectors in an array
      flowfield[index] = v;

//to make the x-offset change over time
      xoff += inc;

    }
//to make the y-offset change over time
    yoff += inc;

//To make the vectors change over time
    zoff += 0.0003;
  }

//For-loop that executes all functions on all of the particles
  for (var i = 0; i < particles.length; i++) {
    particles[i].follow(flowfield);
    particles[i].update();
    particles[i].edges();
    particles[i].show();
  }
}
