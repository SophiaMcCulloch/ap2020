let words;
let myfont;


function preload(){
  words = loadJSON("words.json");
  myfont = loadFont("Truetypewriter.ttf");
}



function setup(){
  createCanvas(windowWidth,windowHeight);
}



function draw(){
//Draw stops looping so only one poem is generated
noLoop();

//calling the loaded font to be used
textFont(myfont);

textSize(25);
fill(0);

//The first phrase of the poem where each word is randomly called from the json file
text(random(words.nouns) + " " + random(words.verbs) + " " + random(words.nouns),100,170);

//The second phrase of the poem where each word is randomly called from the json file
text(random(words.time) + " " + random(words.adjs) + " " + random(words.nouns2)+ " " + random(words.verbs3),100,230);

//The third phrase of the poem --
text(random(words.adjs) + " " + random(words.nouns2) + " " + random(words.verbs2),100,290);

}

//When the mouse is pressed the canvas is cleared and a new poem appears
function mousePressed() {
  clear();
  loop();
}
